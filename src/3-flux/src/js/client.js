import React, { Component } from 'react';
import ReactDOM from "react-dom";
import { Router, Route, IndexRoute, hashHistory } from "react-router";

import Favorites from "./pages/Favorites";
import Todos from "./pages/Todos";
import Layout from "./pages/Layout";
import Settings from "./pages/Settings";

export default class ReactFlux extends Component {
  render() {
    return (
      <Router history={hashHistory}>
        <Route path="/" component={Layout}>
          <IndexRoute component={Todos}></IndexRoute>
          <Route path="favorites" component={Favorites}></Route>
          <Route path="settings" component={Settings}></Route>
        </Route>
      </Router>
    )
  }
}
