import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import BasicReact from "./1-basic-react/client"; // OK
// import ReactRouter from "./2-react-router/src/js/client"; // NOK
// import ReactFlux from "./3-flux/src/js/client";
// import Layout from "./4-redux/src/js/components/Layout";
// import ReactRedux from "./5-redux-react/src/js/client";
// import Layout from "./6-mobx-react/src/js/client";
// import Root from './7-redux-advance/containers/Root'

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <BasicReact />
      </div>
    );
  }
}

export default App;

